# ImagePoster

## Description

ImagePoster is a bot which posts an image daily at a specified time. Images are
organized into sequences. A subscription to a sequence defines which image to
post next, as well as when to post it.

## Image Sequences

Let I be a set of images. You'd like to send them to a channel with a set
frequency. To do that, the set must be ordered into a sequence. Let S(I) be a
seqence of images from set I. S(I) should have the following properties:

- S(I) should include every image from I exactly once.

The easiest way of turning I into S(I) is ordering I by hand. More sophisticated
algorithms can be used, for example, an algorithm which takes into account the
quality of images.

Problem: think about an ordering algorithm.

Each image sequence will be represented by a json file with the following
format:

```json
{
    "Name": "Engineering memes",
    "Images": [
        "image1.png",
        "http://site.com/image2.png",
        "image3.jpg"
    ]
}
```

`Name` is the name of the sequence. `Images` can contain either filepaths or
urls of images. The ID of a sequence will be equal to the filename of the
sequence file without the extensions, eg. `sequence` for `sequence.json`. The
bot will scan for sequence files on startup. The directories to be scanned will
be specified in the `SEQUENCE_PATH` environment variable.  Directories in the
variable will be separated by `:`. If the variable is empty or not set, the bot
act as if `SEQUENCE_PATH` was set to
`/etc/imageposter:/usr/local/etc/imageposter:~/.local/etc/imageposter:~/.config/imageposter`.

A sequence file will be parsed into the following structure:

```
struct Sequence {
    string sequence_id;
    string sequence_name;
    string[] image_sequence;
}
```

## Subscriptions

A Subscription is a one of concepts used by the bot. A user can subscribe to an
image sequence on a channel. The bot will then post images to that channel. A
Subscription must allow the following operations:

- a subscription can be started,
- a subscription can be ended.

Thus, the subscription should have the following properties:

- `guild_id`
- `channel_id`
- `sequence_id`
- `seq_index`
- `timestamp`

(`guild_id`, `channel_id`, `sequence_id`) is the primary UID of a subscription.
The `seq_index` is an index which points at an image to post next. `timestamp`
is the time of day when an image is posted. It is specified with an accuracy to
a minute.

A subscription ends when all images of a sequence are posted. If sequence used
by a subscription is deleted, subscriptions which depend on it stop being
updated and instead post information that the sequence is missing.


## Bot commands

The bot should have the following commands:

- `sequences` - lists the available sequences,
- `subscriptions` - lists the subscriptions on this channel,
- `subscribe <sequence id> <time of day>`, where:
    - `sequence id` and id of a sequence, defined in section "Image Sequences",
    - `time of day` is a timestamp in the form `[0-9][0-9]:[0-9][0-9]`.
- `unsubscribe' <sequence id>` - ends a subscription to the specified sequence.
- `post` - posts an image from a sequence and increments the subscription
   counter.
- `reschedule <sequence id> <time of day>` - changes the time at which an image
   is posted.
