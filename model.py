#!/usr/bin/python3

from sqlalchemy import (MetaData, Table, Column, PrimaryKeyConstraint,
                        UniqueConstraint, String, Integer, Time)

metadata = MetaData()

subs = Table("Subscriptions", metadata,
             Column("subscription_id", Integer,
                    nullable=False, autoincrement=True),
             Column("guild_id", Integer, nullable=False),
             Column("channel_id", Integer, nullable=False),
             Column("sequence_code", String(256), nullable=False),
             Column("sequence_index", Integer, nullable=False),
             Column("timestamp", Time, nullable=False),
             PrimaryKeyConstraint("subscription_id"),
             UniqueConstraint("guild_id", "channel_id", "sequence_code"))


class Sequence():
    def __init__(self, name, elements):
        self.name = name
        self.elements = elements

    def __repr__(self):
        return f"Sequence(description='{self.name}')"


class Subscription():
    def __init__(self, sub_id, guild_id, channel_id, sequence_code, index,
                 timestamp):
        self.sub_id = sub_id
        self.guild_id = guild_id
        self.channel_id = channel_id
        self.sequence_code = sequence_code
        self.index = index
        self.timestamp = timestamp

    def __repr__(self):
        return (f"Sub(id = {self.sub_id}, guild_id = {self.guild_id}, "
                f"channel_id = {self.channel_id}, "
                f"sequence_code = {self.sequence_code}, "
                f"index = {self.index}, "
                f"timestamp = {self.timestamp})")
