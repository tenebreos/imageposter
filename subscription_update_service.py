#!/usr/bin/python3

import datetime
import asyncio

from subscription_service import SubscriptionService
from sequence_service import SequenceService


class SubscriptionUpdateService:

    def __init__(self, subscription_service: SubscriptionService,
                 sequence_service: SequenceService):
        self.sub_service = subscription_service
        self.seq_service = sequence_service
        self.update_tasks = {}

    def set_bot(self, bot):
        self.bot = bot

    async def send_and_increment_sub(self, sub, interaction=None):
        sequence = self.seq_service.get_sequence(sub.sequence_code)
        next_image = sequence.elements[sub.index]
        self.sub_service.set_index(sub.sub_id, (sub.index + 1) %
                                   len(sequence.elements))
        await self.bot.send_image(next_image, sub.channel_id,
                                  interaction=interaction)

    async def update_subscription(self, sub_id):
        sub = self.sub_service.get_subscription(sub_id)
        del self.update_tasks[sub_id]
        if sub is None:  # Subscription was deleted
            return
        await self.send_and_increment_sub(sub)
        print(f"Updating subscription '{sub_id}'")
        update_task = asyncio.create_task(self.update_in(24*60*60, sub_id))
        self.update_tasks[sub_id] = update_task

    async def update_in(self, delay, sub_id):
        await asyncio.sleep(delay)
        await self.update_subscription(sub_id)

    async def schedule_initial_update(self, sub):
        update_timestamp = (datetime.datetime.now()
                            .replace(hour=sub.timestamp.hour,
                                     minute=sub.timestamp.minute,
                                     second=sub.timestamp.second,
                                     microsecond=sub.timestamp.microsecond)
                            .timestamp())
        now_timestamp = datetime.datetime.now().timestamp()
        delta_t = update_timestamp - now_timestamp
        if delta_t < 0:
            delta_t = 24*60*60 + delta_t
        print(f"Sub '{sub.sequence_code}' scheduled in {delta_t/60} minutes.")
        update_task = asyncio.create_task(self.update_in(delta_t, sub.sub_id))
        self.update_tasks[sub.sub_id] = update_task

    async def schedule_updates(self):
        subs = self.sub_service.get_all_subscriptions()
        for sub in subs:
            await self.schedule_initial_update(sub)
