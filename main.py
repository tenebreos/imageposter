#!/usr/bin/python3

import os
import discord
from dotenv import load_dotenv
from sqlalchemy import create_engine

from image_poster import ImagePoster
from sequence_service import SequenceService
from subscription_service import SubscriptionService
from subscription_update_service import SubscriptionUpdateService
from model import metadata

# Load the environment
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
default_sequence_path = ':'.join(["/etc/imageposter",
                                  "/usr/local/etc/imageposter",
                                  "~/.local/etc/imageposter",
                                  "~/.config/imageposter"])
SEQUENCE_PATH = os.getenv('SEQUENCE_PATH', default=default_sequence_path)

# Initialize SQL
engine = create_engine('sqlite:///ImagePoster.db', echo=True, future=True)
metadata.create_all(engine)

# Create services
sequence_service = SequenceService(SEQUENCE_PATH)
subscription_service = SubscriptionService(metadata, engine)
subscription_update_service = SubscriptionUpdateService(subscription_service,
                                                        sequence_service)

# Create and run the bot
intents = discord.Intents.default()
intents.message_content = True
bot = ImagePoster(command_prefix='!', intents=intents, sequences=sequence_service,
                  subscription_service=subscription_service,
                  subscription_update_service=subscription_update_service)
subscription_update_service.set_bot(bot)
bot.run(TOKEN)
