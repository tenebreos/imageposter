#!/usr/bin/python3

# validation.py: predicates for validating data.

import re


def is_valid_time_of_day(time_of_day):
    """
    @brief A predicate for validating time_of_day. time_of_day has the
    following syntax: [num] num ':' num num. Where num is [0-9].
    @returns True iff `time_of_day` is a valid time_of_day.
    """
    regex = r'^[0-9]{1,2}:[0-9]{2}$'
    if not re.match(regex, time_of_day):
        return False
    h, m = time_of_day.split(':')
    m = int(m)
    h = int(h)
    return m >= 0 and m < 60 and h >= 0 and h < 24


def is_url(s):
    """
    @breif A predicate for validating URLs.
    @returns True iff `s` is an URL.
    """
    regex = re.compile(
        r'^(?:http|ftp)s?://'  # http:// or https://
        # domain...
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'
        r'localhost|'  # localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
        r'(?::\d+)?'  # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)
    return not not re.match(regex, s)
