#!/usr/bin/python3

from datetime import datetime

from sqlalchemy import (select, insert, delete, update)

from model import Subscription


class SubscriptionService():
    def __init__(self, metadata, engine):
        self.metadata = metadata
        self.engine = engine
        self.subs = metadata.tables['Subscriptions']

    def get_subscriptions(self, channel_id):
        with self.engine.connect() as conn:
            result = conn.execute(select(self.subs)
                                  .where(self.subs.c.channel_id == channel_id))
            return list(map(_row_to_sub, result))

    def get_all_subscriptions(self):
        with self.engine.connect() as conn:
            result = conn.execute(select(self.subs))
            return list(map(_row_to_sub, result))

    def get_subscription(self, sub_id):
        with self.engine.connect() as conn:
            row = conn.execute(select(self.subs)
                               .where(self.subs.c.subscription_id == sub_id)).first()
            if row is None:
                return None
            return _row_to_sub(row)

    def create_subscription(self, guild_id, channel_id, sequence_code,
                            time_of_day):
        """
        @brief Creates a new subscription and returns it as an object.
        @param guild_id - discord snowflake, uint64
        @param channel_id - discord snowflake, uint64
        @param sequence_code - str
        @param time_of_day - str in "MM:HH" format
        @returns A Subscription object of the newly created subscription.
        """
        timestamp = _time_of_day_to_time(time_of_day)
        with self.engine.begin() as conn:
            result = conn.execute(insert(self.subs)
                                  .values(guild_id=guild_id,
                                          channel_id=channel_id,
                                          sequence_code=sequence_code,
                                          sequence_index=0,
                                          timestamp=timestamp))
        return self.get_subscription(result.inserted_primary_key[0])

    def delete_subscription(self, channel_id, sequence_code):
        with self.engine.begin() as conn:
            conn.execute(delete(self.subs)
                         .where(self.subs.c.channel_id == channel_id)
                         .where(self.subs.c.sequence_code == sequence_code))

    def subscription_exists(self, channel_id, sequence_code):
        with self.engine.connect() as conn:
            result = conn.execute(select(self.subs)
                                  .where(self.subs.c.channel_id == channel_id,
                                         self.subs.c.sequence_code ==
                                         sequence_code)).all()
            n = len(result)
            assert n == 0 or n == 1
            return n != 0

    def find_subscription(self, channel_id, sequence_code):
        with self.engine.connect() as conn:
            row = conn.execute(select(self.subs)
                               .where(self.subs.c.channel_id == channel_id,
                                      self.subs.c.sequence_code ==
                                      sequence_code)).first()
            if row is None:
                return None
            return _row_to_sub(row)

    def set_index(self, sub_id, i):
        with self.engine.begin() as conn:
            conn.execute(update(self.subs)
                         .where(self.subs.c.subscription_id == sub_id)
                         .values(sequence_index=i))

    def set_time(self, sub_id, time_of_day):
        timestamp = _time_of_day_to_time(time_of_day)
        with self.engine.begin() as conn:
            conn.execute(update(self.subs)
                         .where(self.subs.c.subscription_id == sub_id)
                         .values(timestamp=timestamp))


def _row_to_sub(row):
    return Subscription(row.subscription_id,
                        row.guild_id,
                        row.channel_id,
                        row.sequence_code,
                        row.sequence_index,
                        row.timestamp)


def _time_of_day_to_time(time_of_day):
    return datetime.strptime(time_of_day, "%H:%M").time()
