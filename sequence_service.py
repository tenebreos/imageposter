#!/usr/bin/python3

import json
import os

from model import Sequence


class SequenceService():
    def __init__(self, sequence_path):
        self.sequence_path = sequence_path
        self.sequences = _LoadSequences(sequence_path)

    def get_sequences(self):
        return self.sequences

    def exists_sequence(self, sequence_code):
        return sequence_code in self.sequences.keys()

    def get_sequence(self, seq_code):
        return self.sequences[seq_code]


def _LoadSequence(sequence_file):
    file = open(sequence_file.path)
    sequence = json.load(file)
    file.close()
    sequence_name = sequence['Name']
    print(f'- {sequence_name}')
    return Sequence(sequence_name, sequence['Images'])


def _LoadSequences(SEQUENCE_PATH):
    print("# Sequences loaded:")
    sequence_dirs = map(lambda d: os.path.expanduser(d),
                        SEQUENCE_PATH.split(':'))
    sequences = {}
    for d in sequence_dirs:
        if os.path.exists(d):
            for f in os.scandir(d):
                if f.is_file() and f.name.endswith(".json"):
                    seq_code = f.name.split('.')[0]
                    sequences[seq_code] = _LoadSequence(f)
    return sequences
