#!/usr/bin/python3

from discord.ext import commands
from discord import File
from command_cog import CommandCog

from validation import is_url


class ImagePoster(commands.Bot):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sequence_service = kwargs['sequences']
        self.subscription_service = kwargs['subscription_service']
        self.subscription_update_service = kwargs['subscription_update_service']

    async def setup_hook(self):
        await self.add_cog(CommandCog(self, self.sequence_service,
                                      self.subscription_service,
                                      self.subscription_update_service))
        await self.tree.sync()
        await self.subscription_update_service.schedule_updates()

    async def send_image(self, image_path, channel_id, interaction=None):
        """
        @brief sends an image to a channel.
        @param image_path a file path or a URL of the file to send
        @param channel_id id of the channel to send the image to
        """
        if interaction is not None:
            send_proxy = interaction.followup
        else:
            send_proxy = self.get_channel(channel_id)

        if is_url(image_path):
            await send_proxy.send(image_path)
        else:
            file = File(image_path)
            await send_proxy.send(file=file)
