#!/usr/bin/python3

from discord import (Interaction, app_commands)
from discord.ext import commands
from validation import is_valid_time_of_day

_sequence_code_desc = ('Name of a sequence, available from /sequences')
_err_invalid_time_of_day = ("Invalid time_of_day: '{time_of_day}'. "
                            "It should have the following format: HH:MM.")
_err_sequence_does_not_exist_msg = ("Sequence '{sequence_code}' doesn't exist."
                                    " Please, select one from /available.")
_err_not_subscribed = ("This channel is not subscribed to '{sequence_code}'.")


class CommandCog(commands.Cog):
    def __init__(self, bot, sequence_service, subscription_service,
                 subscription_update_service):
        self.bot = bot
        self.sequence_service = sequence_service
        self.subscription_service = subscription_service
        self.subscription_update_service = subscription_update_service

    @app_commands.command(description='List available image sequences')
    async def sequences(self, interaction: Interaction):
        """
        A bot command which prints available sequences.
        """

        message_body = "Available sequences:\n"
        for sid, seq in self.sequence_service.get_sequences().items():
            message_body += f"- {sid}: {seq.name}\n"

        await interaction.response.send_message(_tomarkdown(message_body))

    @app_commands.command(description='List subscriptions of this channel')
    async def subscriptions(self, interaction: Interaction):
        """
        A bot command which prints active subscriptions for a channel.
        """
        await interaction.response.defer()

        cid = interaction.channel_id
        subs = self.subscription_service.get_subscriptions(cid)
        if len(subs) == 0:
            response = "This channel is not subscribed to any sequences."
        else:
            response = "Subscriptions:\n"
            for sub in subs:
                sequence = self.sequence_service.get_sequence(
                    sub.sequence_code)
                n = len(sequence.elements)
                time_of_day = sub.timestamp.strftime('%H:%M')
                response += (f"- {sub.sequence_code}: {sub.index}/{n} images "
                             f"sent, sending at: {time_of_day}\n")

        await interaction.followup.send(_tomarkdown(response))

    @app_commands.command(description='Subscribe to a sequence')
    @app_commands.rename(sequence_code='sequence',
                         time_of_day='time_of_day_in_utc')
    @app_commands.describe(sequence_code=_sequence_code_desc,
                           time_of_day=('Time of day at which an image will be'
                                        ' posted'))
    async def subscribe(self, interaction: Interaction,
                        sequence_code: str, time_of_day: str):
        """
        A bot command which subscribes to a sequence.
        """
        # TODO: add autocompletion
        await interaction.response.defer()

        guild_id = interaction.guild_id
        channel_id = interaction.channel_id

        if not is_valid_time_of_day(time_of_day):
            response = _err_invalid_time_of_day.format(time_of_day=time_of_day)
        elif not self.sequence_service.exists_sequence(sequence_code):
            response = _err_sequence_does_not_exist_msg.format(
                sequence_code=sequence_code)
        elif self.subscription_service.subscription_exists(channel_id,
                                                           sequence_code):
            response = ("This channel is already subscribed to " +
                        f"'{sequence_code}'!")
        else:
            sub = self.subscription_service.create_subscription(guild_id,
                                                                channel_id,
                                                                sequence_code,
                                                                time_of_day)
            await self.bot.subscription_update_service.schedule_initial_update(sub)
            response = ("You have subscribed!")

        await interaction.followup.send(_tomarkdown(response))

    @app_commands.command(description='Ends a subscription.')
    @app_commands.rename(sequence_code='sequence')
    @app_commands.describe(sequence_code=_sequence_code_desc)
    async def unsubscribe(self, interaction: Interaction,
                          sequence_code: str):
        """
        A bot command which ends a subscription.
        """
        # TODO: add autocompletion
        await interaction.response.defer()

        sequence_exists = self.sequence_service.exists_sequence(sequence_code)

        if not sequence_exists:
            response = (f"Sequence '{sequence_code}' does not exist.")
            await interaction.followup.send(_tomarkdown(response))
            return

        channel_id = interaction.channel_id
        sub_exists = self.subscription_service.subscription_exists(channel_id,
                                                                   sequence_code)

        if not sub_exists:
            response = _err_not_subscribed.format(sequence_code=sequence_code)
        else:
            self.subscription_service.delete_subscription(interaction.channel_id,
                                                          sequence_code)
            response = ("Unsubscribed successfully!")

        await interaction.followup.send(_tomarkdown(response))

    @app_commands.command(description=('Post an image from a sequence.'))
    @app_commands.rename(sequence_code='sequence')
    @app_commands.describe(sequence_code=_sequence_code_desc)
    async def post(self, interaction: Interaction,
                   sequence_code: str):
        """
        A bot command which posts an image the next image in a subscription.
        """
        await interaction.response.defer()

        sequence_exists = self.sequence_service.exists_sequence(sequence_code)

        if not sequence_exists:
            response = (f"Sequence '{sequence_code}' does not exist.")
            await interaction.followup.send(_tomarkdown(response))
            return

        channel_id = interaction.channel_id
        sub = self.subscription_service.find_subscription(channel_id,
                                                          sequence_code)
        if sub is None:
            response = _err_not_subscribed.format(sequence_code=sequence_code)
            await interaction.followup.send(_tomarkdown(response))
        else:
            await self.bot.subscription_update_service.send_and_increment_sub(
                sub, interaction=interaction)

    @app_commands.command(description=('Change the posting time of a '
                                       'subscription.'))
    @app_commands.rename(sequence_code='sequence',
                         time_of_day='time_of_day_in_utc')
    @app_commands.describe(sequence_code=_sequence_code_desc)
    async def reschedule(self, interaction: Interaction, sequence_code: str,
                         time_of_day: str):
        """
        A command for changing the posting time of a subscribtion.
        """
        await interaction.response.defer()

        sequence_exists = self.sequence_service.exists_sequence(sequence_code)

        if not sequence_exists:
            response = (f"Sequence '{sequence_code}' does not exist.")
            await interaction.response.send_message(_tomarkdown(response))
            return

        channel_id = interaction.channel_id
        sub = self.subscription_service.find_subscription(channel_id,
                                                          sequence_code)
        if sub is None:
            response = _err_not_subscribed.format(sequence_code=sequence_code)
        elif not is_valid_time_of_day(time_of_day):
            response = _err_invalid_time_of_day.format(time_of_day=time_of_day)
        else:
            self.subscription_service.set_time(sub.sub_id, time_of_day)
            print("# update tasks:")
            self.subscription_update_service.update_tasks[sub.sub_id].cancel()
            rescheduled_sub = self.subscription_service.get_subscription(
                sub.sub_id)
            await self.subscription_update_service.schedule_initial_update(rescheduled_sub)
            response = (f"Time of posting of '{sequence_code}' has been " +
                        f"changed to {time_of_day}.")

        await interaction.followup.send(_tomarkdown(response))


def _tomarkdown(s):
    return '```' + s + '```'
